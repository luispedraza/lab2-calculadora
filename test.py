import unittest 

from calculadora import Calculadora

class CalculadoraTest(unittest.TestCase):

	def test_suma(self):
		# arrange
		calculadora = Calculadora()
		n1 = 5
		n2 = 10
		rEsperado = 15
		# act
		rObtenido = calculadora.suma(n1, n2)

		# assert
		self.assertEqual(rObtenido, rEsperado)


	def test_suma2(self):
		# arrange
		calculadora = Calculadora()
		valores = [[1, 3, 4],[0, 2, 2]]
		resultados = []

		# act
		for v in valores:
			resultados.append(calculadora.suma(v[0], v[1]))


		# assert
		for i, r in enumerate(resultados):
			self.assertEqual(r, valores[i][2])

if __name__ == "__main__":
	unittest.main()
